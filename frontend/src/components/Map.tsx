import {Map, InfoWindow, Marker, GoogleApiWrapper} from "google-maps-react";
import * as React from "react";

interface MyProps {
    google: any;
    cities: any;
}

interface MyState {
    isOpen: boolean;
}

class MapContainer extends React.Component<MyProps, MyState> {
    private map: any;
    public constructor(props) {
        super(props);
        this.state = {
            isOpen: false,
        };
        this.handleClick = this.handleClick.bind(this);
        this.handleClose = this.handleClose.bind(this);
    }
    public render() {
        return (
            <Map
                id="map"
                google={this.props.google}
                zoom={3.5}
                initialCenter={{
                    lat: 8.7832,
                    lng: 8.7832,
                }}
            >
                {this.props.cities.map((city) =>
                    <Marker
                        key={city.id}
                        id={city.name}
                        position={{lat: city.latitude, lng: city.longitude}}
                        onClick={this.handleClick(city)}
                    >
                        <InfoWindow
                            onCloseClick={this.handleClose}
                        >
                            <span>{city.name}</span>
                        </InfoWindow>
                    </Marker>,
                )}
            </Map>
        );
    }

    private handleClose() {
        this.setState({isOpen: false});
    }

    private handleClick(city) {
        // this.setState({isOpen: true});
        const map = document.getElementById("map");
        if (map !== null) {
            const infoWindow = new InfoWindow({
                google: this.props.google,
                content: city.name,
                Position: {lat: city.latitude, lng: city.longitude},
            });
            infoWindow.open(map);
        }
    }
}

export default GoogleApiWrapper({
    apiKey: (process.env.YOUR_GOOGLE_API_KEY_GOES_HERE),
})(MapContainer);
