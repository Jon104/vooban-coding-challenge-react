import * as React from "react";
import SearchBar from "material-ui-search-bar";
import GoogleMap from "../components/Map";
import AudioPlayer from "react-audio-player";
import TableCities from "../containers/TableCities";
import { debounce } from "lodash";

class SearchCities extends React.Component<any, any> {
  private search: any;
  public constructor(props) {
    super(props);
    this.handleInputChange = this.handleInputChange.bind(this);

    this.state = {userLocation: { lat: 8, lng: 8 }};
  }

  public componentDidMount() {
    this.search = debounce(this.props.getCities, 250);

    navigator.geolocation.getCurrentPosition(
      (position) => {
        const { latitude, longitude } = position.coords;

        this.setState({
          userLocation: { lat: latitude, lng: longitude },
      });
    });
    // tslint:disable-next-line:no-console
    console.log(this.state.userLocation);
  }

  public render(): JSX.Element {
    const isVisible = this.props.cities && this.props.cities.length > 0 ? "is-success" : "is-invisible";

    return(
      <div className="hero is-fullheight">
        <div className="hero-header">
          <GoogleMap
            cities={this.props.cities}
          />

          <div className="container">
              <SearchBar
                placeholder="Search a city..."
                onChange={(newValue) => this.handleInputChange(newValue)}
              />

              <div id="table" className={isVisible}>
                <TableCities />
            </div>
          </div>
        </div>
      </div>
    );
  }

  private setTableVisibility(query: string) {
    const showTableResults = query.length > 2;
    const tableVisibility = showTableResults ? "Visible" : "Hidden";
    document.getElementById("table").style.visibility = tableVisibility;
  }

  private async handleInputChange(newValue) {
    this.setTableVisibility(newValue);
    if (newValue.length > 2) {
      await this.search(newValue);
    }
  }
}

export default SearchCities;
