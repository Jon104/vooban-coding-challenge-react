import * as React from "react";
import {
    Table,
    TableHead,
    TableBody,
    TableCell,
    TableFooter,
    TablePagination,
    TableRow,
} from "@material-ui/core";
import Paper from "@material-ui/core/Paper";
import { City } from "../actions/types";

class TableCities extends React.Component<any, any> {
    public constructor(props) {
        super(props);
    }

    public render(): JSX.Element {
        return(
            <Paper>
                <Table>
                    <TableHead>
                        <TableRow style={{height: 30}}>
                            <TableCell>City</TableCell>
                            <TableCell>Latitude</TableCell>
                            <TableCell>Longitude</TableCell>
                        </TableRow>
                    </TableHead>

                    <TableBody>
                        {this.props.cities.map((city) =>
                        <TableRow
                            key={city.id}
                            style={{height: 30}}
                        >
                            <TableCell>{`${city.city}`}</TableCell>
                            <TableCell>{`${city.latitude}`}</TableCell>
                            <TableCell>{`${city.longitude}`}</TableCell>
                        </TableRow>,
                        )}
                    </TableBody>
                </Table>
            </Paper>
        );
    }
}

export default TableCities;
