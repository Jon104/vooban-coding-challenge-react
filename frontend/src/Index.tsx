import * as React from "react";
import * as ReactDOM from "react-dom";
import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";
import { grey800, pink800 } from "material-ui/styles/colors";
import getMuiTheme from "material-ui/styles/getMuiTheme";
import SearchCities from "./containers/SearchCitiesContainers";
import { connect, Provider } from "react-redux";
import {
  BrowserRouter,
  Route,
} from "react-router-dom";

import "./axios";
import { store } from "./store";
// import "../scss/app.scss";

const muiTheme = getMuiTheme({
  fontFamily: "Roboto, sans-serif",
  palette: {
    accent1Color: grey800,
    primary1Color: pink800,
  },
});

ReactDOM.render(
  <Provider store={store}>
    <MuiThemeProvider muiTheme={muiTheme}>
      <div className="is-fullheight">
        <BrowserRouter>
          <Route path="/" title={"SearchCities"} component={SearchCities} />
        </BrowserRouter>
      </div>
    </MuiThemeProvider>
  </Provider>,
  document.getElementById("app"),
);
