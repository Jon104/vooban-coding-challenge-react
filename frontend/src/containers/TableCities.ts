import { connect } from "react-redux";
import TableCities from "../components/TableCities";

const mapStateToProps = (state) => { const { cities } = state.cities; return { cities }; };

export function mapDispatchToProps() {
    return {
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(TableCities);
