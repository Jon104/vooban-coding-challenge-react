import { connect, Dispatch } from "react-redux";
import { bindActionCreators } from "redux";
import SearchCities from "../components/SearchCities";
import * as actions from "../actions/Search";

const mapStateToProps = (state) => { const { cities } = state.cities; return { cities }; };

export function mapDispatchToProps(dispatch: Dispatch<actions.GetCities>) {
    return {
        getCities: async (query) => {
            dispatch(await actions.getCities(query));
        },
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(SearchCities);
