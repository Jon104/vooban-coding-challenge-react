import { applyMiddleware, combineReducers, createStore } from "redux";
import thunk from "redux-thunk";
import { CitiesReducer } from "./reducers/CitiesReducer";

const store = createStore(
    combineReducers({
        cities: CitiesReducer,
    }),
    applyMiddleware(thunk),
);
export {store};
