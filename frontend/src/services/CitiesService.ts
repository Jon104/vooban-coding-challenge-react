import axios from "axios";

export const getCities = async (query: string) => {
    const { data } = await axios.get("/geo/cities", {
        params: {
            namePrefix: query,
        },
    });

    return data;
};
