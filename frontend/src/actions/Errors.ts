interface ThrowError {
    customMessage: string;
    error: object;
    type: "THROW_ERROR";
}

export function throwError(customMessage, error): ThrowError {
    return {
        customMessage,
        error,
        type: "THROW_ERROR",
    };
}
