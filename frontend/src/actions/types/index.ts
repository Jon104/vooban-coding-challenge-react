export const GET_CITIES = "GET_CITIES";

export interface City {
    id: number;
    name: string;
    lattitude: number;
    longitude: number;
}

export interface Cities {
    data: {
        items: City[];
    };
}
