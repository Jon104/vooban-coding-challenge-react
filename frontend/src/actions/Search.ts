import { getCities } from "../services/CitiesService";
import { GET_CITIES, Cities } from "./types";

export interface GetCities {
        cities: Cities;
}

const GetCitiesAction = async (query) => {
        const cities = await getCities(query);
        return {
                type: GET_CITIES,
                payload: cities.data,
        };
};

export { GetCitiesAction as getCities };
